﻿using UnityEngine;

public class BottomZone : MonoBehaviour {

    public GameObject ball;
    public GameObject ballSpawnPoint;

    private Rigidbody rb;

    void Start()
    {
        rb = ball.GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision other)
    {
        GameManager.LoseLife(true);
        BallController.ballInPlay = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        ball.transform.position = ballSpawnPoint.transform.position;
    }
}
