﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Breakout
{

    public abstract class BreakoutAbstractClass : MonoBehaviour
    {

        public static int lives = 3;
        public int bricks = 15;
        public GameObject gameOver;
        public GameObject youWon;
        public GameObject bat;
        public GameObject ball;

        public static bool isGameOver = false;

        public abstract void OnAwake();

        private int resetLives = 3;

        // Use this for initialization
        void Awake()
        {
            OnAwake();
        }

        public void CheckGameOver()
        {
            if (bricks < 1)
            {
                youWon.SetActive(true);
                youWon.GetComponent<Animation>().Play();
                Time.timeScale = .25f;
                isGameOver = true;
            }

            if (lives <= 0)
            {
                gameOver.SetActive(true);
                gameOver.GetComponent<Animation>().Play();
                Time.timeScale = .25f;
                isGameOver = true;
            }
        }

        public void DestroyBrick()
        {
            bricks--;
            CheckGameOver();
        }

        public void Reset()
        {
            isGameOver = false;
            lives = resetLives;
            Scene currentScene = SceneManager.GetActiveScene();
            Time.timeScale = 1f;
            SceneManager.LoadScene(currentScene.name);
        }
    }
}

