﻿using UnityEngine;
using UnityEngine.UI;
using Breakout;


public class GameManager : BreakoutAbstractClass {

    public Text livesText;
    public static GameManager _instance;

    public override void OnAwake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    

    public static Lives LoseLife(bool b)
    {
            return LoseLife(b, Lives.Integer());
    }

    /// <summary>
    /// To define how much life is to be deducted - set first parameter to false and then define an int otherwise set first parameter to true
    /// </summary>
    /// <param name="B"></param>
    /// <returns></returns>
    public static Lives LoseLife(bool B, int I)
    {
        if (B)
        {
            lives--;
            _instance.livesText.text = "Lives: " + lives;
            _instance.CheckGameOver();
            
        }
        else if(!B && I > 0)
        {
            lives -= I;
            _instance.livesText.text = "Lives: " + lives;
            _instance.CheckGameOver();
        }
        return null;
    }
    /// <summary>
    /// This instantiates a Particle depending on paramters it can destroy in secs specified
    /// 
    /// Set B to true if the Particle GameObject is to be destroyed. F in secs to be destroyed.
    /// </summary>
    /// <param name="B"></param>
    /// <param name="F"></param>
    /// <param name="G"></param>
    /// <param name="T"></param>
    /// <param name="Q"></param>
    public void GameObjectMechanic(GameObject G, Transform T, Quaternion Q, bool B, float F)
    {
        GameObject tempParticle = Instantiate(G, T.position, Q);

        if (B)
        {
            Destroy(tempParticle, F);
        }
    }
}
