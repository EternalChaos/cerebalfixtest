﻿using UnityEngine;

public class PlayerInput : MonoBehaviour {

    public float paddleSpeed = 0.2f;
    private Vector3 playerPos = new Vector3(0, 0, 0);

    void Update()
    {
        float xPos = transform.position.x + (Input.GetAxis("Horizontal") * paddleSpeed);
        playerPos = new Vector3(Mathf.Clamp(xPos, -5f, -1f), -4.8f, 0f);
        transform.position = Vector3.MoveTowards(transform.position, playerPos, 1f);
    }
}
