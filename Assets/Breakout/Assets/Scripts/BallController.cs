﻿using UnityEngine;

public class BallController : MonoBehaviour {

    //private float startingVelocity;
    
    private Rigidbody rb;
    public static bool ballInPlay = false;

    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(Breakout.BreakoutAbstractClass.lives < 2)
        {
            BallBehaviour(300);
        }
        else
        {
            BallBehaviour(400);
        }
    }

    public void BallBehaviour(float startingVelocity)
    {
        if (Input.GetButtonDown("Fire1") && ballInPlay == false)
        {
            transform.parent = null;
            ballInPlay = true;
            rb.isKinematic = false;
            rb.AddForce(new Vector3(startingVelocity, startingVelocity, 0));
        }

        if (GameManager.isGameOver)
        {
            rb.useGravity = false;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
}
