﻿using UnityEngine;

public class Bricks : MonoBehaviour {

    public GameObject brickParticle;

    void OnCollisionEnter(Collision other)
    {
        GameManager._instance.GameObjectMechanic(brickParticle, transform, Quaternion.identity, true, 1.2f);
        GameManager._instance.DestroyBrick();
        Destroy(gameObject);
    }
}